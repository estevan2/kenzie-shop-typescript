import { Redirect, Route as ReactDOMRoute } from "react-router-dom";
import { useAuth } from "../providers/Auth";
import { useCart } from "../providers/Cart";
import { RouteAuthData } from "../types";
// Se a rota for privada e o usuário não ta logado, ele vai pro login
// Se a rota for privada e o usuário logado, ele vai pra rota
// Se a rota não for privada e o usuário estiver logado, ele não precisa ver
// Se a rota não rota for privada e o usuário não estiver logado, ele pode ver

// true true = ok
// true false = vai pro login
// false e true = dashboard
// false e false = ok

const Route = ({
  isPrivate = false,
  component: Component,
  ...rest
}: RouteAuthData) => {
  const { token } = useAuth();
  const { cart } = useCart();

  return (
    <ReactDOMRoute
      {...rest}
      render={() => {
        return !isPrivate ||
          (isPrivate === true && !!token && cart.length > 0) ? (
          <Component />
        ) : cart.length > 0 ? (
          <Redirect
            to={{
              pathname: isPrivate === true && !!token ? "/dashboard" : "/login",
            }}
          />
        ) : (
          <Redirect
            to={{
              pathname: "/cart",
            }}
          />
        );
      }}
    />
  );
};

export default Route;
