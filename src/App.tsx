import React from "react";
import Routes from "./routes";
import GlobalStyles from "./styles/global";
import Header from "./components/Header";

function App() {
  return (
    <>
      <Header />
      <Routes />
      <GlobalStyles />
    </>
  );
}

export default App;
