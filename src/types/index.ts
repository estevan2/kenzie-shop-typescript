import { Dispatch, ReactNode, SetStateAction } from "react";
import { History } from "history";

export interface AuthProviderData {
  token: string;
  setAuth: Dispatch<SetStateAction<string>>;
  signIn: (
    userData: UserData,
    setError: Dispatch<SetStateAction<boolean>>,
    history: History
  ) => void;
}

export interface Props {
  children: ReactNode;
}

export interface UserData {
  email: string;
  password: string;
}

export interface Product {
  id?: number;
  name: string;
  description: string;
  image_url: string;
  price: number;
  priceFormatted?: number;
}

export interface CartProviderData {
  cart: Product[];
  setCart: Dispatch<React.SetStateAction<Product[]>>;
}

export interface RouteAuthData {
  component: () => JSX.Element;
  path?: string;
  exact?: boolean;
  isPrivate?: boolean;
}
