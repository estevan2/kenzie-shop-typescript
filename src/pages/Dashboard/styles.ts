import styled from "styled-components";

export const Container = styled.div`
  margin-top: 1rem;
  text-align: center;

  .title {
    margin: 1rem;
    margin-top: 5rem;
  }
  .back_button {
    margin: 1rem;
  }
`;
