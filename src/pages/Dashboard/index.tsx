import { Button } from "@material-ui/core";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useCart } from "../../providers/Cart";
import { Container } from "./styles";

const Dashboard = () => {
  const { setCart } = useCart();

  const history = useHistory();

  useEffect(() => {
    setCart([]);
  }, []);

  return (
    <Container>
      <h3 className="title">Compra realizada!</h3>
      <Button
        className="back_button"
        variant="contained"
        color="primary"
        onClick={() => history.push("/")}
      >
        Voltar
      </Button>
    </Container>
  );
};

export default Dashboard;
