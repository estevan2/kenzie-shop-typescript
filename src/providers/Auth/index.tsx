import {
  createContext,
  SetStateAction,
  useContext,
  useState,
  Dispatch,
} from "react";
import api from "../../services/api";
import { Props, AuthProviderData, UserData } from "../../types";
import { History } from "history";

const AuthContext = createContext<AuthProviderData>({} as AuthProviderData);

export const AuthProvider = ({ children }: Props) => {
  const token = localStorage.getItem("token") || "";
  const [auth, setAuth] = useState<string>(token);

  const signIn = (
    userData: UserData,
    setError: Dispatch<SetStateAction<boolean>>,
    history: History
  ) => {
    api
      .post("/login", userData)
      .then((response) => {
        localStorage.setItem("token", response.data.accessToken);
        setAuth(response.data.accessToken);
        history.push("/dashboard");
      })
      .catch((err) => setError(true));
  };

  return (
    <AuthContext.Provider value={{ token: auth, setAuth, signIn }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
