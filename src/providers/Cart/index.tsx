import { createContext, useContext, useEffect, useState } from "react";
import { CartProviderData, Product, Props } from "../../types";

const CartContext = createContext<CartProviderData>({} as CartProviderData);

export const CartProvider = ({ children }: Props) => {
  const strCart = localStorage.getItem("cart") || "";

  const [cart, setCart] = useState<Product[]>(
    strCart !== "" ? JSON.parse(strCart) : []
  );

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  return (
    <CartContext.Provider value={{ cart, setCart }}>
      {children}
    </CartContext.Provider>
  );
};

export const useCart = () => useContext(CartContext);
