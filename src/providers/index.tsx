import { Props } from "../types";
import { AuthProvider } from "./Auth";
import { CartProvider } from "./Cart";

const Providers = ({ children }: Props) => {
  return (
    <AuthProvider>
      <CartProvider>{children}</CartProvider>
    </AuthProvider>
  );
};

export default Providers;
